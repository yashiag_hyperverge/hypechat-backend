import { model, Schema, Document, Model } from "mongoose";
import { Message } from "../utils/types";

const messageSchema: Schema = new Schema({
  sender: { type: Schema.Types.ObjectId, ref: "User" },
  receiver: { type: Schema.Types.ObjectId, ref: "User" },
  data: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: "unseen", //seen unseen
  },
  date: {
    default: new Date(),
    type: Date,
  },
});

export const messageModel = model<Message & Document>("Message", messageSchema);
