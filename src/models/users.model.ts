import { model, Schema, Document, Model } from "mongoose";
import { User } from "../utils/types";

const userSchema: Schema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    status:{
      type:String,
      default:"offline"
    },
    lastSeen: {
      type: Date,
      default: Date.now(),
    },
  }
  // {
  //   timestamps: true,
  //   collection: "users",
  // }
);

export const userModel = model<User & Document>("User", userSchema);
