import * as Mongoose from "mongoose";
import * as dotenv from "dotenv";
import { DatabaseError } from "./utils/errors";

dotenv.config();

// mongodb url to connect to atlas 

const uri = process.env.URI || `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.c7wfbjj.mongodb.net/?retryWrites=true&w=majority`;

Mongoose.set("strictQuery", false);
export const connectDB = async () => {
  try {
  await Mongoose.connect(uri);
  console.log("Connected to database");
  }
  catch (error) { 
    throw new DatabaseError("Database connection error");
  }
  
};

export const disconnect = () => {
  Mongoose.disconnect();
};
