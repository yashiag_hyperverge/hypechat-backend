import { Request, Response } from "express";
import { addMessageService, retrieveChatservice,updateMessageServices } from "../services/messages";

export const addMessage = async (req: Request, res: Response) => {
  const userId = req.body.user._id;
  const receiverId = req.params.receiverId;

  const { data } = req.body;

  const message = await addMessageService(userId, receiverId, data);
  res.status(201).json(message);
};
export const updateMessages = async (req: Request, res: Response) => {
  console.log(req.body,req.params)
  const userId = req.body.user._id;
  const receiverId = req.params.receiverId;
  await updateMessageServices(userId, receiverId);
  res.status(200).send();
};

export const retrieveChat = async (req: Request, res: Response) => {
  const userId = req.body.user._id;
  const secondUserId = req.params.userId;

  const chats = await retrieveChatservice(userId, secondUserId);
  res.status(200).json(chats);
};
