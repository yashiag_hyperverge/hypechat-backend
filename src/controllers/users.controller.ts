import { NextFunction, Request, Response } from "express";
import { BasicResponse} from "../utils/types";
import { addUserService, validateUserService, getAllUsers } from "../services/users.service";
import { getAuthUserData } from "../databases/queries/user.query";

export const addUser = async (req: Request, res: Response, next: NextFunction) => {
	const { name, email, password } = req.body;
	const result: BasicResponse = await addUserService(name, email, password);
	res.status(result.status).send(result);
};

export const loginUser = async (req: Request, res: Response, next: NextFunction) => {
	const { email, password } = req.body;
	const result: BasicResponse = await validateUserService(email, password);
	res.status(result.status).send(result);
};

export const authenticateUser = async (req: any, res: Response, next: NextFunction) => {
	const id = req.body.user;
	const user = await getAuthUserData(id, "-password");
	res.status(200).send(user);
};

export const listAllUsers =  async (req: any, res: Response, next: NextFunction) =>{
	const users = await getAllUsers();
	res.status(200).send(users);
}
