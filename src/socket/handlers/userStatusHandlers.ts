export const userStatusHandlers = (io: any, socket: any) => {
  const joinServer = (payload: any) => {
    console.log(`user1 ${payload.userId} is active now`);
    socket.join(payload.userId);
    socket.broadcast.emit("new_user", {
      update: `user ${payload.userId} is active now`,
      userId: payload.userId,
      status: "joined",
    });
  };

  const setStatusOnline = (payload: { userId: string }) => {
    console.log("set status online");
    socket.broadcast.emit("statusOnline", {
      update: `status set online for user ${payload.userId}`,
    });
  };

  socket.on("join", joinServer);
  socket.on("setStatusOnline", setStatusOnline);
};
