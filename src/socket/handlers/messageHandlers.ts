export const messageHandlers = (io: any, socket: any) => {
	const sendMessage = (payload: { senderId: string; receiverId: string }) => {
		console.log("message received")
		socket
			.to(payload.receiverId)
			.emit("receive_message", {
				update: `new message arrived from user ${payload.senderId}`,
				senderId: payload.senderId,
			});
    };
    const updateMessageSeen = (payload: { seenBy: string; sentBy: string }) => {
    //    console.log("seen",payload)
		socket
			.to(payload.sentBy)
			.emit("inform_message_seen", {
				update: `messages seen by ${payload.seenBy}`,
				senderId: payload.seenBy,
			});
    };

    socket.on("message_seen", updateMessageSeen);
	socket.on("send_message", sendMessage);
};
