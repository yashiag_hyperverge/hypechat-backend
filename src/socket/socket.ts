import { userStatusHandlers } from "./handlers/userStatusHandlers";
import { messageHandlers } from "./handlers/messageHandlers";

export const onConnection = (io: any, socket: any) => {

  socket.emit("online-users", { onlineUsers: socket.onlineUsers });
  console.log("online-users",socket.onlineUsers)


  if (socket.handshake.query.userId) {
    console.log(`[new] user ${socket.handshake.query.userId} is active now`);    
    socket.join(socket.handshake.query.userId);
  }
	userStatusHandlers(io, socket);
  messageHandlers(io, socket);
    
  socket.on("new-user-joined", (name: any) => {
    console.log("Joined : ", name);
    socket.broadcast.emit("user-joined", name);
  });
};
