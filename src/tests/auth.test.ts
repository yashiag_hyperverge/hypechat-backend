import { ClientError } from "../utils/errors";
import { userModel } from "../models/users.model";
import { validateUserService } from "../services/users.service";
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

let email = "yashi@gmail.com";
let password = "abcd";

let user = {
  _id: "123",
  email: email,
  password: password,
};
describe("auth", () => {
  afterEach(() => {
    jest.resetAllMocks();
  });
  it("authenticate user", async () => {
    userModel.findOne = await jest.fn().mockReturnValue(user);
    bcrypt.compare = await jest.fn().mockReturnValue(true);
    jwt.sign = jest
      .fn()
      .mockReturnValue({ status: 200, data: { token: "token" } });
    const result: any = await validateUserService(email, password);
    expect(result.status).toBe(200);
  });
});
