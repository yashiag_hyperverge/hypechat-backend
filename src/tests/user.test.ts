const jsonwebtoken = require("jsonwebtoken");
import { checkIfUserExists, addUser } from "../databases/queries/user.query";
import { addUserService } from "../services/users.service";
import { signToken } from "../services/sign.service";
import dotenv from "dotenv";
import { ClientError } from "../utils/errors";
dotenv.config();

jest.mock("../databases/queries/user.query");

jest.mock("../services/sign.service");

describe("user", () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it("add user when exists", async () => {
    const checkIfUserExistsMock = checkIfUserExists as jest.Mock;
    let name = "yashi";
    let email = "yashi@gmail.com";
    let password = "abcd";
    const payload: any = {
      _id: "12",
      name: name,
      email: email,
      password: password,
    };
    // console.log(checkIfUserExists.mockImplementation());

    checkIfUserExistsMock.mockImplementation(() => Promise.resolve(payload));
    try {
      const resp = await addUserService(name, email, password);
    } catch (err) {
      expect(err).toBeInstanceOf(ClientError);
    }

    expect(addUser).not.toHaveBeenCalled();
    expect(signToken).not.toHaveBeenCalled();

    // expect(resp).toMatchObject({
    //   status: 400,
    //   data: { error: "User already exists" },
    // });
    // addUserMock.mockImplementation(() => Promise.resolve(payload));
  });

  it("add user when user does not exists", async () => {
    const checkIfUserExistsMock = checkIfUserExists as jest.Mock;
    const addUserMock = addUser as jest.Mock;
    const signTokenMock = signToken as jest.Mock;

    let name = "yashi";
    let email = "yashi@gmail.com";
    let password = "abcd";
    const payload: any = {
      _id: "12",
      name: name,
      email: email,
      password: password,
    };
    const mockToken = "random-token";
    // console.log(checkIfUserExists.mockImplementation());

    checkIfUserExistsMock.mockImplementation(() => Promise.resolve(null));
    addUserMock.mockImplementation(() => Promise.resolve(payload));
    signTokenMock.mockImplementation(() => Promise.resolve(mockToken));

    const resp = await addUserService(name, email, password);

    expect(resp).toMatchObject({ status: 201, data: { token: mockToken } });
  });
});
