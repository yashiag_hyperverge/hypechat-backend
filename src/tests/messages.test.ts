const jsonwebtoken = require("jsonwebtoken");
import { ClientError, ServerError } from "../utils/errors";
import { saveMessageToDb } from "../databases/messages";
import { addMessageService, retrieveChatservice } from "../services/messages";
import { messageModel } from "../models/messages.model";

describe("messages", () => {
  afterEach(() => {
    jest.resetAllMocks();
  });
  test("add message", async () => {
    const payload: any = {
      sender: "63d3ac62c119f1fe528bfdd5",
      receiver: "63d3ac7fc119f1fe528bfdd9",
      data: "heii",
      status: "unseen",
    };
    messageModel.prototype.save = jest.fn().mockResolvedValue(payload);
    try {
      const resp: any = await addMessageService(
        payload.sender,
        payload.receiver,
        payload.data
      );
      expect(resp.sender).toEqual(payload.sender);
    } catch (err) {
      expect(err).toBeInstanceOf(ClientError);
    }
  });

  // Testing Get all bus
  test("list message -  returns all message", async () => {
    messageModel.find = jest.fn().mockResolvedValue([{}]);
    try {
      const result: any = await retrieveChatservice;
      expect(result).toBe(expect.any(Array));
    } catch (err: any) {
      expect(err).toBeInstanceOf(ServerError);
    }
  });
});
