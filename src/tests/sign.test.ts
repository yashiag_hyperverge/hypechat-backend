const jsonwebtoken = require("jsonwebtoken");
import { signToken } from "../services/sign.service";
import dotenv from "dotenv";
dotenv.config();

jest.mock("jsonwebtoken", () => ({
  ...jest.requireActual("jsonwebtoken"),
  sign: jest.fn().mockReturnValue("mocked-token"),
}));

describe("signToken", () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it("signs token with correct payload, secret and expires in time", () => {
    let payload;
    payload = {
      user: {
        _id: "123",
      },
    };

    const mockSign = jsonwebtoken.sign;
    const result = signToken(payload.user);
    expect(result).toBe("mocked-token");
    expect(mockSign).toHaveBeenCalledWith(payload, process.env.SECRET_KEY, {
      expiresIn: process.env.TOKEN_EXPIRE_TIME,
    });
  });
});
