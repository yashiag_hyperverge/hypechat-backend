import { messageModel } from "../models/messages.model";
import { ClientError, InputValidationError } from "../utils/errors";
import { userPresent } from "./queries/user.query";

export const saveMessageToDb = async (message: any) => {
  const senderId = message.sender;
  const receiverId = message.receiver;
  const sender = await userPresent(senderId);
  const receiver = await userPresent(receiverId);
  if (!sender || !receiver) {
    throw new ClientError("Enter valid user");
  }
  const newMessage = await message.save();
  return newMessage;
};
export const updateMessageToDb = async (message: any) => {
  const senderId = message.sender;
  const receiverId = message.receiver;
  const sender = await userPresent(senderId);
  const receiver = await userPresent(receiverId);
  if (!sender || !receiver) {
    throw new ClientError("Enter valid user");
  }
  await messageModel.updateMany({
    $or: [
      { $and: [{ sender: senderId }, { receiver: receiverId }] },
      { $and: [{ sender: receiverId }, { receiver: senderId }] },
    ],
  },{status:"seen"});
};

export const findChats = async (userId: string, secondUserId: string) => {
  const user = await userPresent(userId);
  const secondUser = await userPresent(secondUserId);
  if (!user || !secondUser) {
    throw new ClientError("Enter valid user");
  }
  const chats = await messageModel
    .find({
      $or: [
        { $and: [{ sender: userId }, { receiver: secondUserId }] },
        { $and: [{ sender: secondUserId }, { receiver: userId }] },
      ],
    })
    .sort({ date: 1 });
  return chats;
};

