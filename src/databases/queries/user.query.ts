import * as bcrypt from "bcryptjs";
import { userModel } from "../../models/users.model";

export const getAuthUserData = async (id: string, selectedFields: string) => {
  try {
    return await userModel.findById(id).select(selectedFields);
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const checkIfUserExists = async (email: String) => {
  try {
    return await userModel.findOne({ email });
  } catch (err) {
    throw new Error("error occoured");
  }
};

export const addUser = async (
  name: string,
  email: string,
  password: string
) => {
  try {
    const hashedPassword = await bcrypt.hash(password, 12);
    const newUser = new userModel({
      name,
      email,
      password: hashedPassword,
    });
    return newUser.save();
  } catch (err) {
    throw new Error("error occoured");
  }
};

export const userPresent = async (userId: string) => {
  try {
    return await userModel.findById(userId);
  } catch (err) {
    throw new Error("error occoured");
  }
};

export const findAllUsers = async () => {
  try {
    return await userModel.find().select(" -password");
  } catch (err) {
    throw new Error("error occoured");
  }
};

export const updateLastSeen = async (id:string) => {
   try {
    return await userModel.findByIdAndUpdate(id,{lastSeen:Date.now()});
  } catch (err) {
    throw new Error("error occoured");
  }
};