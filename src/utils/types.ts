import { Document, PopulatedDoc } from "mongoose";

export interface User {
  _id?: string;
  name: string;
  email: string;
  password: string;
  status?: string;
  lastSeen?: Date;
}

export interface Message {
  _id: string;
  sender: PopulatedDoc<User & Document>;
  receiver: PopulatedDoc<User & Document>;
  data: string;
  status: string;
  date: Date;
}

export interface BasicResponse {
  status: number;
  message?: string;
  data: {};
}
