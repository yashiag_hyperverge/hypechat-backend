import { Request, Response, NextFunction } from "express";
import {
  checkIfUserExists,
  addUser,
  findAllUsers,
} from "../databases/queries/user.query";
import * as bcrypt from "bcryptjs";
import { ClientError } from "../utils/errors";
import { signToken } from "./sign.service";
import { updateLastSeen } from "../databases/queries/user.query";

export const checkIfPasswordMathches = (
  password: string,
  validPassword: string
) => {
  const doMatch = bcrypt.compare(password, validPassword);
  return doMatch;
};

export const validateUserService = async (email: string, password: string) => {
  const user = await checkIfUserExists(email);
  if (!user) {
    throw new ClientError("Invalid credentials");
  } else {
    const doMatch = await checkIfPasswordMathches(password, user.password);
    if (!doMatch) {
      throw new ClientError("Invalid credentials");
    } else {
      const token = await signToken(user);
      return {
        status: 200,
        data: {
          token: token,
          name: user.name,
          email: user.email,
          id: user._id,
        },
      };
    }
  }
};

export const addUserService = async (
  name: string,
  email: string,
  password: string
) => {
  const user = await checkIfUserExists(email);
  if (user) {
    throw new ClientError("User already exists");
  } else {
    const newUser = await addUser(name, email, password);
    const token = await signToken(newUser);
    return {
      status: 201,
      message: "user added successfully",
      data: {
        token: token,
        name: newUser.name,
        email: newUser.email,
        id: newUser._id,
      },
    };
  }
};

export const getAllUsers = async () => {
  const users = findAllUsers();
  return users;
};

export const updateUserLastSeen = async (id:string) => {
  await updateLastSeen(id);
};