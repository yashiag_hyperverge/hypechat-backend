import { messageModel } from "../models/messages.model";
import {
  ClientError,
  InputValidationError,
  ServerError,
} from "../utils/errors";
import { saveMessageToDb, findChats, updateMessageToDb } from "../databases/messages";

export const addMessageService = async (
  userId: string,
  receiverId: string,
  data: string
) => {
  if (!data) throw new InputValidationError("plz add message");
  if (!receiverId) throw new InputValidationError("choose valid receiver");

  const message = new messageModel({
    sender: userId,
    receiver: receiverId,
    data: data,
  });
  try {
    const newMessage = await saveMessageToDb(message);
    return newMessage;
  } catch (err: any) {
    throw new ClientError("msg could not be saved");
  }
};

export const updateMessageServices = async (
  userId: string,
  receiverId: string,
) => {
  if (!receiverId) throw new InputValidationError("choose valid receiver");

  try {
    await updateMessageToDb({
    sender: userId,
    receiver: receiverId
  });
    return;
  } catch (err: any) {
    throw new ClientError("msg could not be saved");
  }
};

export const retrieveChatservice = async (
  userId: string,
  secondUserId: string
) => {
  try {
    if (!userId || !secondUserId || userId === secondUserId)
      throw new ClientError("Choose valid user");
    const chats = await findChats(userId, secondUserId);
    return chats;
  } catch (err) {
    throw new ServerError("server error");
  }
};
