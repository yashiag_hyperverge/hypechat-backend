import { Router } from "express";
import { authCheck } from "../middlewares/auth.middleware";
// userController contains all the controllers related to the user containing the required logic for the APIs
import {
  loginUser,
  addUser,
  authenticateUser,
  listAllUsers,
} from "../controllers/users.controller";
import { asyncWrap } from "../utils/asyncWrap";

const userRoutes = Router();

// User Registeration api, refer design document for more details on this API
userRoutes.post("/add", asyncWrap(addUser));

//User Login API
userRoutes.post("/login", asyncWrap(loginUser));

//get details of the logged in user
userRoutes.get("/me", authCheck, asyncWrap(authenticateUser));

//get all users
userRoutes.get("/", asyncWrap(listAllUsers));

export default userRoutes;
