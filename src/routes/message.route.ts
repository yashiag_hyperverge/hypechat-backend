import { Router } from "express";
import { addMessage, retrieveChat,updateMessages } from "../controllers/messages";
import { check } from "express-validator";
import { authCheck, adminCheck } from "../middlewares/auth.middleware";
import { validate, asyncWrap } from "../middlewares/error.handler";

const messageRoutes = Router();

// @route POST /booking/:busID
// @desc Book tickets, by selecting seats
// @access Private
messageRoutes.post(
  "/:receiverId",
  [authCheck, check("data").isString().bail().notEmpty()],
  asyncWrap(addMessage)
);

messageRoutes.post(
  "/update/:receiverId",
  authCheck,
  asyncWrap(updateMessages)
);

messageRoutes.get("/:userId", authCheck, asyncWrap(retrieveChat));

export default messageRoutes;
