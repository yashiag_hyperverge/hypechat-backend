import jwt from "jsonwebtoken";
import * as dotenv from "dotenv";
import { RequestHandler, Request, Response, NextFunction } from "express";
import { User } from "../utils/types";
import { userModel } from "../models/users.model";

dotenv.config();

export const authCheck = (req: Request, res: Response, next: NextFunction) => {
  //Get token from header
  const token = req.header("x-auth-token");
  //Check if not token
  if (!token) {
    return res.status(401).json({ msg: "No token, authorization denied" });
  }

  //Verify token
  try {
    const decoded = jwt.verify(token, `${process.env.SECRET_KEY}`);
    if (typeof decoded === "string") {
      return res.status(400).json({ msg: "Token is not valid" });
    }
    userModel.findById(decoded.user._id).then((user) => {
      if (!user) {
        res.status(401).json({ msg: "Token is not valid" });
      } else {
        req.body.user = decoded.user;
        next();
      }
    });
  } catch (err) {
    res.status(401).json({ msg: "Token invalid" });
  }
};

export const adminCheck = (req: Request, res: Response, next: NextFunction) => {
  //Get token from header
  const token = req.header("x-auth-token");

  //Check if not token
  if (!token) {
    return res.status(401).json({ msg: "No token, authorization denied" });
  }

  //Verify token
  try {
    const decoded = jwt.verify(token, `${process.env.SECRET_KEY}`);
    if (typeof decoded === "string") {
      return res.status(400).json({ msg: "Token is not valid" });
    }
    req.body.user = decoded.user;
    userModel.findById(req.body.user._id).then((user) => {
     
      // check email should be of admin only
      if (!user || user.email != "admin@gmail.com") {
        res.status(401).json({ msg: "Token is not valid" });
      } else {
        next();
      }
    });
  } catch (err) {
    res.status(401).json({ msg: "Token is not valid" });
  }
};
