import express from "express";
import * as dotenv from "dotenv";
import { Request, Response, NextFunction } from "express";
import { connectDB } from "./server";
import cookieParser from "cookie-parser";
import userRoutes from "./routes/users.route";
import { sendError } from "./middlewares/error.handler";

import messageRouter from "./routes/message.route";

var cors = require("cors");
const app = express();
const port: Number = parseInt(process.env.PORT || "3000");

const http = require("http");
const server = http.createServer(app);

const { Server } = require("socket.io");
const io = new Server(server, {
  cors: {
    origin: "*",
  },
});

import { onConnection } from "./socket/socket";
import { updateUserLastSeen } from "./services/users.service";

dotenv.config();
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use("/message", messageRouter);
app.use("/user", userRoutes);

//hanldes all path
app.use("*", (req: Request, res: Response, next: NextFunction) => {
  res.status(404).send("not found");
});

//common error handler
app.use(sendError);

// error handler
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  console.log("Error handler middleware");
  console.log("error is", err);
  res.status(500).send("server error");
});

let onlineMap = new Map();
let onlineUsers: string[] = [];
io.on("connection", (socket: any) => {

  onlineMap.set(socket.id, socket.handshake.query.userId);
  if(!onlineUsers.includes(socket.handshake.query.userId)) onlineUsers.push(socket.handshake.query.userId);
  socket.onlineUsers = onlineUsers;

  onConnection(io, socket);

  socket.on("disconnecting", (reason:any) => {
    const disconnectedUserId = onlineMap.get(socket.id);
    onlineUsers = onlineUsers.filter(id => id !== disconnectedUserId);
    
    console.log(onlineUsers)
    updateUserLastSeen(disconnectedUserId)
    socket.broadcast.emit("online-users",{onlineUsers,disconnectedUserId});
    
  });
});

connectDB()
  .then(() => {
    server.listen(port, () => {
      console.log(`Listening on port: ${port}`);
    });
  })
  .catch((error) => {
    console.log(error);
  });

export default app;
